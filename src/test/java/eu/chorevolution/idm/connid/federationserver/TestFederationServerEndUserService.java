/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package eu.chorevolution.idm.connid.federationserver;

import eu.chorevolution.idm.connid.federationserver.api.FederationServerEndUserServicePathAnnotated;
import eu.chorevolution.securitytokenservice.federationserver.api.EndUser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import org.identityconnectors.common.CollectionUtil;
import org.identityconnectors.common.Pair;

public class TestFederationServerEndUserService implements FederationServerEndUserServicePathAnnotated {

    public static final String DEFAULT_USERNAME = "test";

    private final Map<String, EndUser> endusers = new HashMap<>();

    public TestFederationServerEndUserService() {
        EndUser endUser = new EndUser();
        endUser.setUsername(DEFAULT_USERNAME);
        endUser.setPassword("password");
        endUser.setActive(true);
        endUser.getChoreographies().add("wp5");
        endUser.getGroups().add("group1");
        endUser.getGroups().add("group2");
        endUser.getAttributes().put("attr1", CollectionUtil.newSet("value1", "value2"));
        endUser.getAttributes().put("attr2", CollectionUtil.newSet("value3", "value4"));

        Map.Entry<String, String> sc1 = Pair.of("sc1_" + DEFAULT_USERNAME, "password1");
        endUser.getServiceCredentials().put("sc1", sc1);

        Map.Entry<String, String> sc2 = Pair.of("sc2_" + DEFAULT_USERNAME, "password2");
        endUser.getServiceCredentials().put("sc3", sc2);

        this.endusers.put(endUser.getUsername(), endUser);
    }

    @Override
    public Response list(final String domain) {
        return Response.ok(new GenericEntity<List<EndUser>>(new ArrayList<>(endusers.values())) {
        }).build();
    }

    @Override
    public Response create(final String domain, final EndUser enduser) {
        endusers.put(enduser.getUsername(), enduser);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public Response read(final String domain, final String username) {
        EndUser user = endusers.get(username);
        return user == null
                ? Response.status(Response.Status.NOT_FOUND).build()
                : Response.ok(user).build();
    }

    @Override
    public Response update(final String domain, final String username, final EndUser enduser) {
        endusers.remove(username);
        endusers.put(enduser.getUsername(), enduser);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @Override
    public Response delete(final String domain, final String username) {
        endusers.remove(username);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

}
