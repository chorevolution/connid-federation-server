/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package eu.chorevolution.idm.connid.federationserver;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import eu.chorevolution.idm.connid.federationserver.api.FederationServerEndUserServicePathAnnotated;
import java.util.ArrayList;
import java.util.List;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public abstract class AbstractTests {

    public static final String BASE_ADDRESS = "local://federationServer";

    private static Server SERVER;

    @BeforeClass
    public static void startServer() throws Exception {
        JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
        sf.setResourceClasses(FederationServerEndUserServicePathAnnotated.class);

        List<Object> providers = new ArrayList<>();
        providers.add(new JacksonJaxbJsonProvider());
        sf.setProviders(providers);

        sf.setResourceProvider(FederationServerEndUserServicePathAnnotated.class,
                new SingletonResourceProvider(new TestFederationServerEndUserService(), true));
        sf.setAddress(BASE_ADDRESS);

        SERVER = sf.create();
    }

    @AfterClass
    public static void stopServer() throws Exception {
        SERVER.stop();
        SERVER.destroy();
    }

}
