/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.idm.connid.federationserver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.identityconnectors.common.CollectionUtil;
import org.identityconnectors.framework.api.APIConfiguration;
import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.api.ConnectorFacadeFactory;
import org.identityconnectors.framework.common.objects.Attribute;
import org.identityconnectors.framework.common.objects.AttributeBuilder;
import org.identityconnectors.framework.common.objects.AttributeInfo;
import org.identityconnectors.framework.common.objects.AttributeUtil;
import org.identityconnectors.framework.common.objects.ConnectorObject;
import org.identityconnectors.framework.common.objects.Name;
import org.identityconnectors.framework.common.objects.ObjectClass;
import org.identityconnectors.framework.common.objects.ObjectClassInfo;
import org.identityconnectors.framework.common.objects.Schema;
import org.identityconnectors.framework.common.objects.Uid;
import org.identityconnectors.test.common.TestHelpers;
import org.junit.Test;

public class ConnectorTests extends AbstractTests {

    protected FederationServerConfiguration newConfiguration() {
        FederationServerConfiguration conf = new FederationServerConfiguration();
        conf.setBaseAddress(BASE_ADDRESS);
        conf.setDomain("domain");
        return conf;
    }

    protected ConnectorFacade newFacade() {
        ConnectorFacadeFactory factory = ConnectorFacadeFactory.getInstance();
        APIConfiguration impl = TestHelpers.createTestConfiguration(FederationServerConnector.class, newConfiguration());
        impl.getResultsHandlerConfiguration().setFilteredResultsHandlerInValidationMode(true);
        return factory.newInstance(impl);
    }

    @Test
    public void schema() {
        ConnectorFacade connector = newFacade();

        Schema schema = connector.schema();
        assertEquals(1, schema.getObjectClassInfo().size());

        ObjectClassInfo ocinfo = schema.getObjectClassInfo().iterator().next();
        assertEquals(ObjectClass.ACCOUNT_NAME, ocinfo.getType());
        assertEquals(3, ocinfo.getAttributeInfo().size());

        int counter = 0;
        for (AttributeInfo attrinfo : ocinfo.getAttributeInfo()) {
            if (FederationServerConstants.GROUPS_NAME.equals(attrinfo.getName())) {
                assertEquals(String.class, attrinfo.getType());
                counter++;
            } else if (FederationServerConstants.CHOREOGRAPHIES_NAME.equals(attrinfo.getName())) {
                assertEquals(String.class, attrinfo.getType());
                counter++;
            } else if (Name.NAME.equals(attrinfo.getName())) {
                assertTrue(attrinfo.isRequired());
                counter++;
            }
        }
        assertEquals(3, counter);
    }

    @Test
    public void getObject() {
        ConnectorFacade connector = newFacade();
        ConnectorObject enduser = connector.getObject(
                ObjectClass.ACCOUNT, new Uid(TestFederationServerEndUserService.DEFAULT_USERNAME), null);

        assertNotNull(enduser);
        assertEquals(new Uid(TestFederationServerEndUserService.DEFAULT_USERNAME), enduser.getUid());
        assertEquals(new Name(TestFederationServerEndUserService.DEFAULT_USERNAME), enduser.getName());

        assertTrue(AttributeUtil.isEnabled(enduser));

        Attribute groups = enduser.getAttributeByName(FederationServerConstants.GROUPS_NAME);
        assertNotNull(groups);
        assertEquals(2, groups.getValue().size());
        assertTrue(groups.getValue().contains("group1"));
        assertTrue(groups.getValue().contains("group2"));

        Attribute attr1 = AttributeUtil.find("attr1", enduser.getAttributes());
        assertNotNull(attr1);
        assertEquals(2, attr1.getValue().size());
        assertTrue(attr1.getValue().contains("value1"));
        assertTrue(attr1.getValue().contains("value2"));
    }

    @Test
    public void getAllObjects() {
        ConnectorFacade connector = newFacade();

        List<ConnectorObject> list = new ArrayList<>();
        connector.search(ObjectClass.ACCOUNT, null, list::add, null);

        assertNotNull(list);
        assertFalse(list.isEmpty());

        list.stream().forEach((connectorObject) -> {
            assertNotNull(connectorObject);
        });
    }

    private Set<Attribute> getUniqueSample() {
        String uuid = UUID.randomUUID().toString();

        Set<Attribute> attrs = new HashSet<>();
        attrs.add(new Name(uuid));
        attrs.add(AttributeBuilder.buildPassword(uuid.toCharArray()));
        attrs.add(AttributeBuilder.buildEnabled(true));
        attrs.add(AttributeBuilder.build(FederationServerConstants.GROUPS_NAME, CollectionUtil.newSet("group1")));
        attrs.add(AttributeBuilder.build(FederationServerConstants.CHOREOGRAPHIES_NAME, CollectionUtil.newSet()));
        attrs.add(AttributeBuilder.build("attrX", CollectionUtil.newSet("valueX")));
        attrs.add(AttributeBuilder.build("attrY", CollectionUtil.newSet("valueX", "valueY")));

        return attrs;
    }

    @Test
    public void create() {
        ConnectorFacade connector = newFacade();

        Set<Attribute> attrs = getUniqueSample();

        Uid uid = connector.create(ObjectClass.ACCOUNT, attrs, null);
        assertNotNull(uid);
        assertEquals(uid.getUidValue(), AttributeUtil.getNameFromAttributes(attrs).getNameValue());

        ConnectorObject read = connector.getObject(ObjectClass.ACCOUNT, uid, null);
        assertNotNull(read);
        assertEquals(attrs, AttributeUtil.filterUid(read.getAttributes()));
    }

    @Test
    public void update() {
        ConnectorFacade connector = newFacade();

        Set<Attribute> attrs = getUniqueSample();

        Uid uid = connector.create(ObjectClass.ACCOUNT, attrs, null);
        assertNotNull(uid);
        assertEquals(uid.getUidValue(), AttributeUtil.getNameFromAttributes(attrs).getNameValue());

        Attribute attrX = null;
        for (Attribute attr : attrs) {
            if ("attrX".equals(attr.getName())) {
                attrX = attr;
            }
        }
        attrs.remove(attrX);
        attrX = AttributeBuilder.build("attrX", CollectionUtil.newSet("valueZ"));
        attrs.add(attrX);

        uid = connector.update(ObjectClass.ACCOUNT, uid, attrs, null);
        assertNotNull(uid);
        assertEquals(uid.getUidValue(), AttributeUtil.getNameFromAttributes(attrs).getNameValue());

        ConnectorObject read = connector.getObject(ObjectClass.ACCOUNT, uid, null);
        assertNotNull(read);
        assertEquals(attrX, AttributeUtil.find("attrX", read.getAttributes()));

        attrs.clear();
        attrs.add(new Name("updated-" + uid.getUidValue()));
        attrs.add(AttributeBuilder.buildEnabled(false));

        uid = connector.update(ObjectClass.ACCOUNT, uid, attrs, null);
        assertNotNull(uid);
        assertEquals(uid.getUidValue(), AttributeUtil.getNameFromAttributes(attrs).getNameValue());
    }

    @Test
    public void delete() {
        ConnectorFacade connector = newFacade();

        Set<Attribute> attrs = getUniqueSample();

        Uid uid = connector.create(ObjectClass.ACCOUNT, attrs, null);
        assertNotNull(uid);
        assertEquals(uid.getUidValue(), AttributeUtil.getNameFromAttributes(attrs).getNameValue());

        connector.delete(ObjectClass.ACCOUNT, uid, null);

        ConnectorObject read = connector.getObject(ObjectClass.ACCOUNT, uid, null);
        assertNull(read);
    }

}
