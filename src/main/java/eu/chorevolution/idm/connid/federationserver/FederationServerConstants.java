/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.idm.connid.federationserver;

public final class FederationServerConstants {

    public static final String GROUPS_NAME = "__GROUPS__";

    public static final String CHOREOGRAPHIES_NAME = "__CHOREOGRAPHIES__";

    public static final String SERVICE_USERNAME = "Username";

    public static final String SERVICE_PASSWORD = "Password";

    private FederationServerConstants() {
        // private constructor for static utility class
    }
}
