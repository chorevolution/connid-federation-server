/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.idm.connid.federationserver;

import java.net.URI;
import org.identityconnectors.common.StringUtil;
import org.identityconnectors.framework.common.exceptions.ConfigurationException;
import org.identityconnectors.framework.spi.AbstractConfiguration;
import org.identityconnectors.framework.spi.ConfigurationProperty;

public class FederationServerConfiguration extends AbstractConfiguration {

    private String baseAddress;

    private String domain;

    private String principal;

    private String password;

    @ConfigurationProperty(displayMessageKey = "baseAddress.display", helpMessageKey = "baseAddress.help", order = 1)
    public String getBaseAddress() {
        return baseAddress;
    }

    public void setBaseAddress(final String baseAddress) {
        this.baseAddress = baseAddress;
    }

    @ConfigurationProperty(displayMessageKey = "domain.display", helpMessageKey = "domain.help", order = 2,
            required = true)
    public String getDomain() {
        return domain;
    }

    public void setDomain(final String domain) {
        this.domain = domain;
    }

    @ConfigurationProperty(displayMessageKey = "principal.display", helpMessageKey = "principal.help", order = 3)
    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(final String principal) {
        this.principal = principal;
    }

    @ConfigurationProperty(displayMessageKey = "password.display", helpMessageKey = "password.help", order = 4,
            confidential = true)
    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    @Override
    public void validate() {
        if (StringUtil.isBlank(baseAddress)) {
            throw new ConfigurationException("baseAddress must not be blank!");
        } else {
            // verify that baseAddress is a valid URI
            URI.create(baseAddress);
        }

        if (StringUtil.isBlank(domain)) {
            throw new ConfigurationException("domain must not be blank!");
        }

        if (StringUtil.isBlank(principal)) {
            throw new ConfigurationException("principal must not be blank!");
        }
    }

}
