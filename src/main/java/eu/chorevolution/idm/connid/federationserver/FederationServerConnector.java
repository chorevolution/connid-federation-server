/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.idm.connid.federationserver;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import eu.chorevolution.idm.connid.federationserver.api.FederationServerEndUserServicePathAnnotated;
import eu.chorevolution.securitytokenservice.federationserver.api.EndUser;
import eu.chorevolution.securitytokenservice.federationserver.api.FederationServerEndUserService;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.identityconnectors.common.Pair;
import org.identityconnectors.common.StringUtil;
import org.identityconnectors.common.logging.Log;
import org.identityconnectors.framework.common.objects.Attribute;
import org.identityconnectors.framework.common.objects.AttributeBuilder;
import org.identityconnectors.framework.common.objects.AttributeInfo;
import org.identityconnectors.framework.common.objects.AttributeInfoBuilder;
import org.identityconnectors.framework.common.objects.AttributeUtil;
import org.identityconnectors.framework.common.objects.ConnectorObject;
import org.identityconnectors.framework.common.objects.Name;
import org.identityconnectors.framework.common.objects.ObjectClass;
import org.identityconnectors.framework.common.objects.OperationOptions;
import org.identityconnectors.framework.common.objects.OperationalAttributes;
import org.identityconnectors.framework.common.objects.ResultsHandler;
import org.identityconnectors.framework.common.objects.Schema;
import org.identityconnectors.framework.common.objects.SchemaBuilder;
import org.identityconnectors.framework.common.objects.Uid;
import org.identityconnectors.framework.common.objects.filter.AbstractFilterTranslator;
import org.identityconnectors.framework.common.objects.filter.EqualsFilter;
import org.identityconnectors.framework.common.objects.filter.FilterTranslator;
import org.identityconnectors.framework.spi.Configuration;
import org.identityconnectors.framework.spi.Connector;
import org.identityconnectors.framework.spi.ConnectorClass;
import org.identityconnectors.framework.spi.operations.CreateOp;
import org.identityconnectors.framework.spi.operations.DeleteOp;
import org.identityconnectors.framework.spi.operations.SchemaOp;
import org.identityconnectors.framework.spi.operations.SearchOp;
import org.identityconnectors.framework.spi.operations.TestOp;
import org.identityconnectors.framework.spi.operations.UpdateOp;

@ConnectorClass(
        configurationClass = FederationServerConfiguration.class,
        displayNameKey = "federationserver.connector.display")
public class FederationServerConnector implements Connector,
        CreateOp, UpdateOp, DeleteOp,
        SchemaOp, TestOp, SearchOp<FederationServerFilter> {

    private static final Log LOG = Log.getLog(FederationServerConnector.class);

    private FederationServerConfiguration configuration;

    @Override
    public FederationServerConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public void init(final Configuration configuration) {
        this.configuration = (FederationServerConfiguration) configuration;
        LOG.ok("Connector {0} successfully inited", getClass().getName());
    }

    @Override
    public void dispose() {
        // dispose of any resources the this connector uses.
    }

    private void checkObjectClass(final ObjectClass objectClass) {
        if (!objectClass.equals(ObjectClass.ACCOUNT)) {
            throw new IllegalArgumentException("Only " + ObjectClass.ACCOUNT.getObjectClassValue() + " is supported");
        }
    }

    private FederationServerEndUserService getClient() {
        JAXRSClientFactoryBean bean = new JAXRSClientFactoryBean();
        bean.setAddress(getConfiguration().getBaseAddress());
        bean.setServiceClass(FederationServerEndUserServicePathAnnotated.class);
        bean.setUsername(configuration.getPrincipal());
        bean.setPassword(configuration.getPassword());
        bean.setProvider(new JacksonJaxbJsonProvider());
        FederationServerEndUserService client =
                bean.create(FederationServerEndUserServicePathAnnotated.class, configuration.getDomain());

        WebClient.client(client).accept(MediaType.APPLICATION_JSON_TYPE).type(MediaType.APPLICATION_JSON_TYPE);
        return client;
    }

    private EndUser getEndUser(final String username, final Set<Attribute> attrs) {
        EndUser endUser = new EndUser();
        endUser.setUsername(username);

        Map<String, Map.Entry<String, String>> serviceCredentials = new HashMap<>();

        attrs.stream().forEach((attr) -> {
            if (attr.getName().equals(OperationalAttributes.PASSWORD_NAME)) {
                if (attr.getValue() != null && !attr.getValue().isEmpty()) {
                    StringBuilder clearPwd = new StringBuilder();
                    AttributeUtil.getGuardedStringValue(attr).access(clearPwd::append);
                    endUser.setPassword(clearPwd.toString());
                }
            } else if (attr.getName().equals(OperationalAttributes.ENABLE_NAME)) {
                if (attr.getValue() != null && !attr.getValue().isEmpty()) {
                    endUser.setActive(((Boolean) attr.getValue().get(0)));
                }
            } else if (attr.getName().equals(FederationServerConstants.CHOREOGRAPHIES_NAME)) {
                if (attr.getValue() != null) {
                    attr.getValue().stream().forEach((value) -> {
                        endUser.getChoreographies().add(value.toString());
                    });
                }
            } else if (attr.getName().equals(FederationServerConstants.GROUPS_NAME)) {
                if (attr.getValue() != null) {
                    attr.getValue().stream().forEach((value) -> {
                        endUser.getGroups().add(value.toString());
                    });
                }
            } else if (attr.getName().endsWith(" " + FederationServerConstants.SERVICE_USERNAME)
                    || attr.getName().endsWith(" " + FederationServerConstants.SERVICE_PASSWORD)) {

                if (attr.getValue() != null) {
                    String serviceName = attr.getName().substring(0, attr.getName().lastIndexOf(' '));
                    String credential = attr.getName().substring(attr.getName().lastIndexOf(' ') + 1);

                    Map.Entry<String, String> credentials = serviceCredentials.get(serviceName);
                    if (credentials == null) {
                        credentials = new Pair<>();
                        serviceCredentials.put(serviceName, credentials);
                    }
                    if (attr.getValue() != null && !attr.getValue().isEmpty()) {
                        if (credential.equals(FederationServerConstants.SERVICE_USERNAME)) {
                            ((Pair<String, String>) credentials).first = attr.getValue().get(0).toString();
                        } else if (credential.equals(FederationServerConstants.SERVICE_PASSWORD)) {
                            ((Pair<String, String>) credentials).second = attr.getValue().get(0).toString();
                        }
                    }
                }
            } else if (!attr.getName().equals(Uid.NAME) && !attr.getName().equals(Name.NAME)) {
                Set<String> values = new HashSet<>();
                if (attr.getValue() != null) {
                    attr.getValue().stream().forEach((value) -> {
                        values.add(value.toString());
                    });
                }
                endUser.getAttributes().put(attr.getName(), values);
            }
        });

        endUser.getServiceCredentials().putAll(serviceCredentials);

        return endUser;
    }

    @Override
    public Uid create(
            final ObjectClass objectClass,
            final Set<Attribute> attrs,
            final OperationOptions options) {

        checkObjectClass(objectClass);

        Name name = AttributeUtil.getNameFromAttributes(attrs);
        if (name == null || name.getValue() == null || name.getValue().isEmpty()) {
            throw new IllegalArgumentException("No " + Name.NAME + " provided");
        }

        getClient().create(getConfiguration().getDomain(), getEndUser(name.getNameValue(), attrs));

        return new Uid(name.getNameValue());
    }

    @Override
    public Uid update(
            final ObjectClass objectClass,
            final Uid uid,
            final Set<Attribute> attrs,
            final OperationOptions options) {

        checkObjectClass(objectClass);

        String updated = null;
        Name name = AttributeUtil.getNameFromAttributes(attrs);
        if (name != null) {
            updated = name.getNameValue();
        }

        if (updated == null) {
            updated = uid.getUidValue();
        }
        getClient().update(getConfiguration().getDomain(), uid.getUidValue(), getEndUser(updated, attrs));

        return new Uid(updated);
    }

    @Override
    public void delete(
            final ObjectClass objectClass,
            final Uid uid,
            final OperationOptions options) {

        checkObjectClass(objectClass);

        getClient().delete(getConfiguration().getDomain(), uid.getUidValue());
    }

    @Override
    public Schema schema() {
        Set<AttributeInfo> attrInfo = new HashSet<>();
        attrInfo.add(AttributeInfoBuilder.build(FederationServerConstants.GROUPS_NAME, String.class));
        attrInfo.add(AttributeInfoBuilder.build(FederationServerConstants.CHOREOGRAPHIES_NAME, String.class));

        SchemaBuilder schemaBuilder = new SchemaBuilder(FederationServerConnector.class);
        schemaBuilder.defineObjectClass(ObjectClass.ACCOUNT.getObjectClassValue(), attrInfo);

        return schemaBuilder.build();
    }

    @Override
    public void test() {
        getConfiguration().validate();
    }

    @Override
    public FilterTranslator<FederationServerFilter> createFilterTranslator(
            final ObjectClass objectClass,
            final OperationOptions options) {

        return new AbstractFilterTranslator<FederationServerFilter>() {

            @Override
            protected FederationServerFilter createEqualsExpression(final EqualsFilter filter, final boolean not) {
                return filter.getAttribute() == null || filter.getAttribute().getValue() == null
                        || filter.getAttribute().getValue().isEmpty()
                        ? super.createEqualsExpression(filter, not)
                        : new FederationServerFilter(filter.getAttribute().getValue().get(0).toString());
            }
        };
    }

    private ConnectorObject getConnectorObject(final EndUser endUser) {
        Set<Attribute> attrs = new HashSet<>();
        attrs.add(new Uid(endUser.getUsername()));
        attrs.add(new Name(endUser.getUsername()));
        if (endUser.getPassword() != null) {
            attrs.add(AttributeBuilder.buildPassword(endUser.getPassword().toCharArray()));
        }
        attrs.add(AttributeBuilder.buildEnabled(endUser.isActive()));

        attrs.add(AttributeBuilder.build(FederationServerConstants.GROUPS_NAME, endUser.getGroups()));
        attrs.add(AttributeBuilder.build(FederationServerConstants.CHOREOGRAPHIES_NAME, endUser.getChoreographies()));

        endUser.getServiceCredentials().entrySet().stream().forEach((entry) -> {
            attrs.add(AttributeBuilder.build(
                    entry.getKey() + " " + FederationServerConstants.SERVICE_USERNAME,
                    entry.getValue().getKey()));
            attrs.add(AttributeBuilder.build(
                    entry.getKey() + " " + FederationServerConstants.SERVICE_PASSWORD,
                    entry.getValue().getValue()));
        });

        endUser.getAttributes().entrySet().stream().forEach((entry) -> {
            attrs.add(AttributeBuilder.build(entry.getKey(), entry.getValue()));
        });

        return new ConnectorObject(ObjectClass.ACCOUNT, attrs);
    }

    @Override
    public void executeQuery(
            final ObjectClass objectClass,
            final FederationServerFilter query,
            final ResultsHandler handler,
            final OperationOptions options) {

        checkObjectClass(objectClass);

        FederationServerEndUserService client = getClient();

        if (query == null || StringUtil.isBlank(query.getUsername())) {
            client.list(getConfiguration().getDomain()).readEntity(new GenericType<List<EndUser>>() {
            }).stream().forEach((endUser) -> {
                handler.handle(getConnectorObject(endUser));
            });
        } else {
            Response response = client.read(getConfiguration().getDomain(), query.getUsername());
            if (response.getStatus() == Response.Status.OK.getStatusCode() && response.hasEntity()) {
                EndUser found = response.readEntity(EndUser.class);
                if (found != null) {
                    handler.handle(getConnectorObject(found));
                }
            }
        }
    }
}
